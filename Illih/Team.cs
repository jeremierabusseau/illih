﻿namespace Illih
{
    public class Team
    {
        public TeamColor Color { get; set; }
        public short Points { get; set; }

        public void IncrementPoints()
        {
            this.Points++;
        }
    }
}