﻿using System.Windows.Controls;

namespace Illih
{
    public static class Switcher
    {
        public static MainWindow PageSwitcher;

        public static void Switch(UserControl newPage)
        {
            PageSwitcher.Navigate(newPage);
        }
    }
}