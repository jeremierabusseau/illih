﻿using System.Windows;
using System.Windows.Controls;
using Illih.Pages;

namespace Illih
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Switcher.PageSwitcher = this;
            Switcher.Switch(new WelcomePage());
        }

        public void Navigate(UserControl nextPage)
        {
            Content = nextPage;
        }
    }
}