﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace Illih.Pages
{
    public partial class GuessMusicPage : UserControl
    {
        private const string Mp3Pattern = @"*.mp3";
        private const short SecondsToGuess = 5;
        private const short DefaultRounds = 10;
        private static readonly string Mp3FolderPath = AppDomain.CurrentDomain.BaseDirectory + @"..\..\Resources\";
        private readonly Team _blueTeam = new Team {Color = TeamColor.Blue};
        private readonly List<string> _categories;
        private readonly List<string> _playedMusics = new List<string>();
        private readonly MediaPlayer _mediaPlayer = new MediaPlayer();
        private readonly DispatcherTimer _playingMusicTimer = new DispatcherTimer {Interval = TimeSpan.FromSeconds(1)};
        private readonly Team _purpleTeam = new Team {Color = TeamColor.Purple};
        private readonly Random _randomizer = new Random();
        private readonly short _roundsToPlay;
        private readonly short _secondsToGuess;
        private short _roundsCounter;

        public GuessMusicPage(List<string> categories, short roundsToPlay = DefaultRounds, short secondsToGuess = SecondsToGuess)
        {
            InitializeComponent();
            _secondsToGuess = secondsToGuess;
            _roundsToPlay = roundsToPlay;
            _categories = categories;
            GoToNextMusic();
        }

        private void GoToNextMusic()
        {
            if (_roundsCounter++ >= _roundsToPlay)
            {
                GoToResultPage();
                return;
            }

            ChooseRandomMusic();
            StartTimer();
            InitializeProgressBar();
            _mediaPlayer.Play();
        }

        private void ChooseRandomMusic()
        {
            while (true)
            {
                var selectedCategory = _categories[_randomizer.Next(_categories.Count)];
                var files = Directory.GetFiles(Mp3FolderPath + selectedCategory + @"\", Mp3Pattern);
                var selectedFile = files[_randomizer.Next(files.Length)];
                var selectedFilePath = new Uri(selectedFile);

                var fileName = Path.GetFileName(selectedFile).Replace(".mp3", string.Empty);
                if (_playedMusics.Contains(fileName))
                {
                    continue;
                }

                _playedMusics.Add(fileName);
                _mediaPlayer.Open(selectedFilePath);

                MusicTitleNameLabel.Content = fileName;
                MusicTitleNameLabel.Visibility = Visibility.Hidden;
                NextButton.Visibility = Visibility.Hidden;
                BlueTeamButton.IsEnabled = false;
                PurpleTeamButton.IsEnabled = false;
                break;
            }
        }

        private void StartTimer()
        {
            _playingMusicTimer.Tick += PlayingMusicTimerTick;
            _playingMusicTimer.Start();
        }

        private void PlayingMusicTimerTick(object sender, EventArgs e)
        {
            BlueTeamButton.IsEnabled = true;
            PurpleTeamButton.IsEnabled = true;
            TimeLeftToGuessProgressBar.Value -= 1;

            if (_mediaPlayer.Position.Seconds <= _secondsToGuess) return;
            MusicTitleNameLabel.Visibility = Visibility.Visible;
            NextButton.Visibility = Visibility.Visible;
        }

        private void GoToResultPage()
        {
            var resultPage = new ResultPage(_blueTeam, _purpleTeam);
            Switcher.Switch(resultPage);
        }

        private void GuessTriggeredByTeam()
        {
            _mediaPlayer.Close();
            _playingMusicTimer.Stop();
            GoToNextMusic();
        }

        private void blueTeam_Click(object sender, RoutedEventArgs e)
        {
            _blueTeam.IncrementPoints();
            GuessTriggeredByTeam();
        }

        private void purpleTeam_Click(object sender, RoutedEventArgs e)
        {
            _purpleTeam.IncrementPoints();
            GuessTriggeredByTeam();
        }

        private void NextButton_OnClick(object sender, RoutedEventArgs e)
        {
            GuessTriggeredByTeam();
        }

        private void InitializeProgressBar()
        {
            TimeLeftToGuessProgressBar.Minimum = 0d;
            TimeLeftToGuessProgressBar.Maximum = _secondsToGuess;
            TimeLeftToGuessProgressBar.Value = _secondsToGuess;
        }
    }
}