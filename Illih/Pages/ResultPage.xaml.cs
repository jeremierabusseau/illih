﻿using System.Windows.Controls;
using System.Windows.Media;

namespace Illih.Pages
{
    public partial class ResultPage : UserControl
    {
        private readonly Team _blueTeam = new Team {Color = TeamColor.Blue};
        private readonly Team _purpleTeam = new Team {Color = TeamColor.Purple};

        public ResultPage(Team blueTeam, Team purpleTeam)
        {
            InitializeComponent();
            var brushConverter = new BrushConverter();

            if (blueTeam.Points >= purpleTeam.Points)
            {
                WinnerText.Text = blueTeam.Points.ToString();
                WinnerText.Background = (Brush) brushConverter.ConvertFrom("#01cdfe");
                LooserText.Text = purpleTeam.Points.ToString();
                LooserText.Background = (Brush) brushConverter.ConvertFrom("#b967ff");
            }
            else
            {
                WinnerText.Text = purpleTeam.Points.ToString();
                WinnerText.Background = (Brush) brushConverter.ConvertFrom("#b967ff");
                LooserText.Text = blueTeam.Points.ToString();
                LooserText.Background = (Brush) brushConverter.ConvertFrom("#01cdfe");
            } 
        }
    }
}