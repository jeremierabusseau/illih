﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Newtonsoft.Json;

namespace Illih.Pages
{
    public partial class WelcomePage : UserControl
    {
        private static readonly string Mp3FolderPath = AppDomain.CurrentDomain.BaseDirectory + @"..\..\Resources\";

        public WelcomePage()
        {
            InitializeComponent();
            Categories = new List<string>();

            var directories = Directory.GetDirectories(Mp3FolderPath);
            var categories = directories.Select(cat => new DirectoryInfo(cat).Name).ToList();
            foreach (var checkBox in categories.Select(item => new CheckBox
                {Name = item, Content = item, IsChecked = true, FontSize = 28, Foreground = Brushes.FloralWhite}))
                CategoriesPanel.Children.Add(checkBox);
        }

        private List<string> Categories { get; }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (CheckBox element in CategoriesPanel.Children)
                if (element.IsChecked != null && (bool) element.IsChecked)
                    Categories.Add(element.Name);

            if (!Regex.IsMatch(Rounds.Text, @"^\d+$") || !Regex.IsMatch(SecondsToGuess.Text, @"^\d+$"))
            {
                return;
            }

            var guessMusicPage = new GuessMusicPage(Categories, short.Parse(Rounds.Text), short.Parse(SecondsToGuess.Text));
            Switcher.Switch(guessMusicPage);
        }


        private void SecondsToGuess_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(SecondsToGuess.Text, "[^0-9]")) return;
            MessageBox.Show("Please enter only numbers.");
            SecondsToGuess.Text = SecondsToGuess.Text.Remove(SecondsToGuess.Text.Length - 1);
        }

        private void Rounds_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(Rounds.Text, "[^0-9]")) return;
            MessageBox.Show("Please enter only numbers.");
            Rounds.Text = Rounds.Text.Remove(Rounds.Text.Length - 1);
        }
    }
}